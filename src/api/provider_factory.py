from rpc.rpc_block_api import RpcBlockApiImpl
from rpc.rpc_reward_api import RpcRewardApiImpl
from dunscan.dunscan_reward_api import DunscanRewardApiImpl
from dunscan.dunscan_block_api import DunScanBlockApiImpl
from Constants import PUBLIC_NODE_URL

class ProviderFactory:

    def __init__(self, provider, verbose=False):
        self.provider = provider
        self.verbose = verbose

    def newRewardApi(self, network_config, baking_address, node_url, node_url_public=''):
        if self.provider == 'rpc':
            if node_url.find("http") == -1:
                node_url = 'http://' + node_url
            return RpcRewardApiImpl(network_config, baking_address, node_url, verbose=self.verbose)
        elif self.provider == 'prpc':
            if node_url_public == '':
                raise Exception("Public RPC was selected as data source, but no public RPC server was provided. Please configure one using -Ap.")
            return RpcRewardApiImpl(network_config, baking_address, node_url_public, verbose=self.verbose)
        elif self.provider == 'dunscan':
            return DunscanRewardApiImpl(network_config, baking_address, verbose=self.verbose)

        raise Exception("No supported reward data provider : {}".format(self.provider))

    def newBlockApi(self, network_config, node_url):
        if self.provider == 'rpc' or self.provider == 'prpc':
            if node_url.find("http") == -1:
                node_url = 'http://' + node_url
            return RpcBlockApiImpl(network_config, node_url)
        elif self.provider == 'dunscan':
            return DunScanBlockApiImpl(network_config)

        raise Exception("No supported reward data provider : {}".format(self.provider))
