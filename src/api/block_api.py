from Constants import BOOTSTRAP_LEVELS
from abc import ABC, abstractmethod
from math import floor

class BlockApi(ABC):
    def __init__(self, nw):
        super(BlockApi, self).__init__()
        self.nw = nw

    @abstractmethod
    def get_current_level(self, verbose=False):
        pass

    def level_to_cycle(self, level):
        if level <= BOOTSTRAP_LEVELS:
            return floor(level - 1 / 64)
        return floor((level - BOOTSTRAP_LEVELS) / self.nw['BLOCKS_PER_CYCLE']) + 64

    def get_current_cycle(self):
        return self.level_to_cycle(self.get_current_level())
