from unittest import TestCase
from cli.wallet_client_manager import WalletClientManager


class TestWalletClientManager(TestCase):
    def test_parse_get_manager_for_contract_response(self):
        response = """
                Random Text for Testing:
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Cras lacinia suscipit maximus. Etiam ultricies tellus nec
            neque euismod bibendum. Nunc eget ante faucibus elit dictum
            viverra ultrices sed lacus. Nullam laoreet aliquet nulla.

            dn1KogjUiBYdkBGvUPdRhKrK2cssRqH7eX63 (known as zonbob)
                """
        clientManager = WalletClientManager(None, node_addr=None)
        manager = clientManager.parse_get_manager_for_contract_response(response)
        self.assertEqual('dn1KogjUiBYdkBGvUPdRhKrK2cssRqH7eX63', manager)

    def test_parse_client_list_known_contracts_response(self):
        response = """
                Random Text for Testing:
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Cras lacinia suscipit maximus. Etiam ultricies tellus nec
            neque euismod bibendum. Nunc eget ante faucibus elit dictum
            viverra ultrices sed lacus. Nullam laoreet aliquet nulla.

            newcontr: KT1XqEHigP5XumZy9i76QyVd6u93VD4HTqJK
            zonbob: dn1cUpC874bfrF1VDcFbuBwNZ951hdtQQgVg
            mainnetme: dn1WaAD9jDpGmAFwxN2SVBkYo3NnuGR9Ep5G
                    """
        clientManager = WalletClientManager(None, node_addr=None)
        dict = clientManager.parse_list_known_contracts_response(response)

        self.assertTrue(dict['newcontr'] == 'KT1XqEHigP5XumZy9i76QyVd6u93VD4HTqJK')
        self.assertTrue(dict['zonbob'] == 'dn1cUpC874bfrF1VDcFbuBwNZ951hdtQQgVg')
        self.assertTrue(dict['mainnetme'] == 'dn1WaAD9jDpGmAFwxN2SVBkYo3NnuGR9Ep5G')

    def test_parse_list_known_addresses_response(self):
        response = """
                Random Text for Testing:
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            Cras lacinia suscipit maximus. Etiam ultricies tellus nec
            neque euismod bibendum. Nunc eget ante faucibus elit dictum
            viverra ultrices sed lacus. Nullam laoreet aliquet nulla.

            mainpay: dn1PgwzYhTWGCzfXwRfMzRTbATUUADSq4Xgc
            zonbob: dn1cUpC874bfrF1VDcFbuBwNZ951hdtQQgVg (unencrypted sk known)
            mainnetme: dn1WaAD9jDpGmAFwxN2SVBkYo3NnuGR9Ep5G (tcp sk known)
            zeronetme: dn1J3zyiNUUH98wvzeLEzXTKPCpkwaf7YYkb (unencrypted sk not known)
            baker: tz1XXXXXXXX (unix sk known)
                    """

        clientManager = WalletClientManager(None, node_addr=None)
        dict = clientManager.parse_list_known_addresses_response(response)

        zonbob = dict['dn1cUpC874bfrF1VDcFbuBwNZ951hdtQQgVg']

        self.assertEqual(zonbob['alias'], 'zonbob')
        self.assertEqual(zonbob['sk'], True)

        mainnetme = dict['dn1WaAD9jDpGmAFwxN2SVBkYo3NnuGR9Ep5G']

        self.assertEqual(mainnetme['alias'], 'mainnetme')
        self.assertEqual(mainnetme['sk'], True)

        zeronetme = dict['dn1J3zyiNUUH98wvzeLEzXTKPCpkwaf7YYkb']

        self.assertEqual(zeronetme['alias'], 'zeronetme')
        self.assertEqual(zeronetme['sk'], False)

        mainpay = dict['dn1PgwzYhTWGCzfXwRfMzRTbATUUADSq4Xgc']

        self.assertEqual(mainpay['alias'], 'mainpay')
        self.assertEqual(mainpay['sk'], False)

        baker = dict['tz1XXXXXXXX']
        self.assertEqual(baker['alias'], 'baker')
        self.assertEqual(baker['sk'], True)
