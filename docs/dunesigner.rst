How to use the Dune Signer
===========================

The Dune signer daemon can be configured to sign operations with the secret key of the payment account.
There are two steps: first import the secret key to the signer, and second. tell the client that it can use the signer to sign operations.

1. Configure Signer
  Replace "`<myaddressalias>`" with your alias.
  Replace "edesk1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
  with your encryped private key. IP address and port
  selection are up to the user.

  ::

      ./dune-signer import secret key <myaddressalias> encrypted:edesk1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      ./dune-signer launch socket signer -a 127.0.0.1 -p 22000 -W

2. Configure Client
  Replace "`<myaddressalias>`" with your alias.
  Replace "`<PKH>`" with your public key hash.
  Use the same host port combination from the previous step.

  ::

    ./dune-client import secret key <myaddressalias> tcp://127.0.0.1:22000/<PKH> -f

When the dune-client is asked to sign an operation, the operation is sent to the signer.
The signer generates a signature and sends it back to the client.
Normally encrypted accounts are imported to the signer so it is necessary to provide encryption password to the signer at launch.
Note that signer generates generic signatures e.g. sigXXXX but not edsigXXXX.
