Payment Address
===============

DRD is designed to work as a linux service.
It expects the usage of the Dune signer for encrypted payment accounts. Unencrypted payment accounts can be used without dune signer, but this is not recommended.
If a payment account is encrypted and not configured to be signed by the dune signer, DRD will freeze.

An address can only be used for payments if it satisfies the
following criteria:

- Public key of the address must be revealed. See the dune command line interface on how to run a reveal command on dune client.
  If an address is registered as delegate, there is no need to run reveal command.

  ::

      ./dune-client reveal key for <alias>

- The secret key of the address must be known and already imported.

- If the secret key is encrypted, dune-signer must be used to sign payments with DRD. For instructions on how to use signer, please refer to the next section.

- Use of unencrypted secret key is also possible. An unencrypted secret key can be imported to the client using the command: 

  ::

      ./dune-client import secret key <alias> unencrypted:edskXXXXX

For more information about the dune command line interface, read the help pages:

  ::

      ./dune-client man -v 3
  